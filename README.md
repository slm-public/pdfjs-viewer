This project can be installed via npm using the following command:
`npm install git+https://gitlab.com/slm-public/pdfjs-viewer.git --save`

To add it to your package.json use the following:
`"pdfjs-viewer": "git+https://gitlab.com/slm-public/pdfjs-viewer.git",`